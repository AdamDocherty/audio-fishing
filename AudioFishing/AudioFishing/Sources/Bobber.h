/*
	File:	Bobber.h
	Date:	5th April 2014.
	Author:	Adam Docherty.

	Based on the My3DSound class provided by Allan C. Milne as part of the solution to the Soundscape3 practical. 

	Requires: XASound, Updateable.
	
	Description:
	The CBobber class represents the end of the fishing line. It can be cast, and reeled in by the player.
	The bobber will play a sound when it lands in the water after it has been cast, and will play a constant sound
	once a fish has been hooked on to the line.

	It implements the Updateable interface to provide the positioning functionality and to start/stop playing the sound.
	A XASound object is used to encapsulate the sound.
	An X3DAudio emitter struct is used to maintain the coordinates of the sound
	with a reference to the listener also being maintained.

*/

#ifndef Bobber_h__
#define Bobber_h__

// system includes.
#include <Windows.h>
#include <XAudio2.h>
#include <X3DAudio.h>
#include <memory>
#include <string>

// Framework includes.
#include "Updateable.hpp"

//--- forward declarations.
namespace AllanMilne { namespace Audio { class XASound; } }
class CSoundRandomiser;

class CBobber : public AllanMilne::Updateable
{
public:
	//--- Enumeration representing all the possible states the bobber can be in
	enum BobberState {
		// the bobber has not been cast, won't reel in.
		BOBBER_STATE_NOT_CAST = 0,
		// the bobber is currently in flight after being cast.
		BOBBER_STATE_FLIGHT,
		// the bobber has been cast, but no fish has yet been hooked
		// reeling in will scare away fish.
		BOBBER_STATE_CAST,
		// the a fish has been hooked on to the line, reel in to catch the fish.
		BOBBER_STATE_FISH_HOOKED
	};

	CBobber(const X3DAUDIO_LISTENER* listener);

	~CBobber() {}

	//--- initalises all sound objects 
	void InitialiseSounds();
	
	//--- Gets the current state of the bobber
	inline BobberState GetState() { return m_state; }

	//--- Casts the bobber to a specified position, also uses the players position to calculate the flight time.
	void Cast(const X3DAUDIO_VECTOR& position );
	//--- yank the rod left
	void YankLeft();
	//--- yank the rod right
	void YankRight();

	//--- sets the reel in speed
	void SetReelInSpeed(float speed);

	//--- getter for the sound position.
	X3DAUDIO_VECTOR& GetPosition () { return m_emitter.Position; }
	const X3DAUDIO_VECTOR& GetPosition () const { return m_emitter.Position; }
	void SetPosition(float x, float y, float z);

	//=== Implementation of the Updatable interface
	virtual void OnUpdate( const float deltaTime );
	virtual void OnUpdateActivation() {}
	virtual void OnUpdateDeactivation() {}

protected:

	enum FishState {
		FISH_STATE_NO_FISH = 0,
		FISH_STATE_STILL,
		FISH_STATE_SWIM_LEFT,
		FISH_STATE_SWIM_RIGHT,
		FISH_STATE_YANK_LEFT,
		FISH_STATE_YANK_RIGHT
	};

	//--- Update the BOBBER_STATE_FLIGHT state
	void UpdateFlight( const float deltaTime );

	//--- Update the BOBBER_STATE_CAST state
	void UpdateCast( const float deltaTime );

	//--- Update the BOBBER_STATE_FISH_HOOKED state
	void UpdateFishHooked( const float deltaTime );

	//--- update the fish attached to the line
	void UpdateFish( const float deltaTime );

	//--- Calculates the time the bobber will stay in the flight state
	void CalculateFlightTime();
	//--- Calculates the reel in direction based off of the current position and they player's position
	void CalculateReelInDirection();

	//--- Called when the bobber lands in the water
	void OnBobberLanded();
	//--- Called when a fish is hooked on to the bobber
	void OnFishHooked();
	//--- called when the rod has been yanked when a fish is on the line
	void OnYanked();
	//--- called when the fish either escapes or is reeled in
	void OnFishUnhooked();
	//--- called when the fish has been caught
	void OnFishCaught();
	//--- called when the fish escapes without being fully reeled in
	void OnFishGotAway();

	//--- Reels the bobber towards the player
	void ReelIn(const float deltaTime);
	//--- rotates the bobber around the player
	void MoveFish(const float angle);

	//--- Called when the line has been fully reeled in
	void OnReeledIn();

	const X3DAUDIO_VECTOR& GetPlayerPosition() const;

	//--- gets the current angle of the bobber relative to north
	float GetCurrentAngle() const;
	//--- gets the difference between the bobber X value and the players X value
	float GetDeltaX() const;
	//--- gets the distance between the player and the bobber
	float GetDistance() const;

	//--- Sound objects
	std::shared_ptr<AllanMilne::Audio::XASound> m_splashSound;
	std::shared_ptr<AllanMilne::Audio::XASound> m_reelSound;
	std::shared_ptr<AllanMilne::Audio::XASound> m_caughtSound;
	std::shared_ptr<AllanMilne::Audio::XASound> m_yankSound;
	std::shared_ptr<AllanMilne::Audio::XASound> m_snapSound;
	std::shared_ptr<CSoundRandomiser> m_fishSound;

	//--- Emitter struct, also stores the position of the bobber
	X3DAUDIO_EMITTER m_emitter;

	//--- Reference to the player's listener struct
	const X3DAUDIO_LISTENER* m_listener;

	//--- Vector containing the x and z velocity components used for reeling in
	X3DAUDIO_VECTOR m_reelInDirection;

	//--- The speed that the bobber is being reeled in
	float m_reelInSpeed;

	//=== flight state member variables
	//--- The time the bobber has remaining in flight
	float m_flightTime;

	//=== cast state member variables
	//--- The time remaining before the fish grabs on to the hook
	float m_waitingTime;

	//--- The current state of the bobber
	BobberState m_state;

	//=== fish variables
	FishState m_fishState;
	//--- the timer used for choosing when the fish should start swimming
	float m_fishTimer;
	//--- the time from when the fish gets yanked back in, until it starts moving again
	float m_fishSwimDelay;

};
#endif // Bobber_h__