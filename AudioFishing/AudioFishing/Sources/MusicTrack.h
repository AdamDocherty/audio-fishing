/*
	File:	MusicTrack.h
	Date:	23rd April 2014.
	Author:	Adam Docherty.
	
	Description:
	Stores the source voice and buffer for a single music track.

	Each music track has it's own source voice to allow for layering of music tracks
	if necessary.

*/

#ifndef MusicTrack_h__
#define MusicTrack_h__

#include <Windows.h>

#define XAUDIO2_HELPER_FUNCTIONS
#include <XAudio2.h>

#include <string>

class CMusicTrack
{
public:
	//--- constructs the music track object with a filename. 
	//--- A loop begin and length can be provided if the track does not loop neatly on it's own
	CMusicTrack(const std::string& filename, UINT32 loopBegin = 0, UINT32 loopLength = 0);
	~CMusicTrack();

	//--- play the audio track, looping
	void Play();
	//--- returns true if the audio track is currently being played
	bool IsPlaying();
	//--- stops the music track
	void Stop();

	//--- sets the volume of the track
	//--- Note volume units are in dB; if you want to use the XAudio2 amplitude multiplier units then access directly via the source voice.
	//--- method does nothing if supplied value is out of XAudio2 allowable range.
	void SetVolume(float volume);

protected:
	//--- the source voice for the music track
	IXAudio2SourceVoice *m_sourceVoice;
	//--- the audio buffer for the music track
	XAUDIO2_BUFFER		m_XABuffer;

	bool				m_paused;
};

#endif // MusicTrack_h__
