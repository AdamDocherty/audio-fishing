/*
	File:	FishingGame.hpp
	Version:	1.0
	Date:	17th February 2014.
	Author:	Adam Docherty

	Modified from the Soundscape3.hpp provided by Allan Milne for the Soundscape3 practical

	Requires:	IState, XASound, C3DSound, Player.
	
	Description:
	
	This is a concrete implementation of the IState interface for the application logic.
	This is the concrete strategy class called  from the context WinCore class as part of a strategy pattern.
	*	See IState.hpp for details of this interface.

	this application plays a soundscape made up of a variety of sound elements
	and allows the user to walk around this soundscape.
	It illustrates the use of XAudio2, X3DAudio, my framework and relevant OO architecturing.

	Sounds include static sounds whose properties don't change and 3D sounds that will appear to move as the user moves.
	The user (listener) is modeled by an X3DAudio listener struct field.
	
	This class exposes a user interface controlling the user walking and turning their head.

	Note this incorporates a flag to indicate whether the object resources have been cleaned up.
	this is to allow both the explicit Cleanup and destructor to be used.

	The Load3DSounds method allows the user to load the sounds from a text file rather than needing to
	recompile when adjusting sounds. The file containing sounds data should be formatted as follows:

		<filepath>
		<x-position>, <y-position>, <z-position>
		<volume>
		<curve distance scaler>
		<Looped or Random> [<random range low> <random range high>]
		// need to enter random ranges if random is selected, values in seconds

	Optionally, there can also be comments anywhere in the file outside of the definition blocks, 
		these comments should have a '#' at the start of the line
	You can have as many 3d sounds objects in this file as you want.
*/

#ifndef FishingGame_h__
#define FishingGame_h__

// system includes.
#include <memory>
#include <vector>

// Framework includes.
#include "IState.hpp"
#include "Player.h"

//--- forward declarations.
namespace AllanMilne { namespace Audio { class XASound; } }
class C3DSound;
class CBoulder;

class FishingGame : public AllanMilne::IState {
public:
	//=== Application specific behaviour.

	//--- Default constructor.
	FishingGame (); 
	
	//--- Destructor - delete all resources allocated by Initialize.
	virtual ~FishingGame();

	//=== Implementation of the IState interface.

	//--- create and initialize the required sounds to be played.
	//--- since this game has no graphics the argument is not used.
	bool Initialize (HWND aWindow);

	//--- Called at the start of processing; displays instructions and plays the sounds.
	bool Setup ();

	//--- process a single application frame.
	//--- Check for key pressess, perform the corresponding changes to the listener position and orientation.
	//--- this will call the relevant Update() functions on all 3D sounds.
	bool ProcessFrame (const float deltaTime);

	//--- Stop the sounds playing and reset listener.
	//--- set the clean up flag to true.
	//--- this will be called by the destructor if not previously called explicitly.
	void Cleanup ();



private:
	//--- loads the 3d sounds from a text file, see header comments for description
	//--- on how the text file should be layed out.
	void Load3DSounds(const std::string& filename);
	//--- loads any 3d sounds which cannot be loaded in from the text file as they have 
	//--- unique properties (e.g. child classes of 3DSound or non-default emitter values)
	void AddUnique3DSounds();
	//--- Handles all inputs using GetASyncKeyState()
	void HandleInputs();
	//--- updates the position of the river sound to be next to the player
	//--- to create the illusion of the sound source being a continuous river
	void UpdateRiverSoundPosition();

	//--- flag to indicate whether resources have already been cleaned up.
	bool m_cleanedUp;

	//--- the static sounds to be played.
	std::unique_ptr<C3DSound> m_riverSound;

	//--- the collection of updateable 3D sounds.
	std::vector<std::unique_ptr<C3DSound>> m_3DSounds;

	//--- the boulder that blocks sounds
	std::unique_ptr<CBoulder> m_boulder;
	
	//--- the player
	std::unique_ptr<CPlayer> m_player;

}; // end FishingGame class.

#endif //FishingGame_h__ 