/*
	File:	ServiceLocator.h
	Date:	23rd April 2014.
	Author:	Adam Docherty.

	Requires: IMusicManager
	
	Description:
	A Service locater containing every service that can be accessed globally
	throughout the program.

	Each service should be represented as a abstract base class with a separate
	implementation to reduce coupling with the services as much as possible.

	Currently only MusicManager can be accessed through this. 

*/

#ifndef ServiceLocator_h__
#define ServiceLocator_h__

#include <memory>

class IMusicManager;

class CServiceLocator
{
public:
	
	//--- retrieves the music manager service from the service locator
	static std::shared_ptr<IMusicManager> getMusicManager();
	//--- provides the music manager service to the Service locator
	static void provide(std::shared_ptr<IMusicManager> service);
	//--- clears the music manager service
	static void clearMusicManager();
private:
	//--- Music manager service, used for controlling what music track is being played
	static std::shared_ptr<IMusicManager> s_musicManagerService;

};

#endif // ServiceLocator_h__
