/*
	File:	3DSound.hpp
	Date:	17th February 2014.
	Author:	Adam Docherty

	The file has been modified by from Allan C. Milne's My3DSound.hpp file 
	to remove the using keyword in the .hpp file, 
	and to adhere to the naming convention for the rest of the project.

	Requires:	XASound, Updateable.
	
	Description:
	This is part of a solution to the Soundscape3 practical.
	
	The C3DSound class models a sound element at a fixed position in a world coordinate system.
	It implements the Updateable interface to provide the positioning functionality and to start/stop playing the sound.
	A XASound object is used to encapsulate the sound.
	An X3DAudio emitter struct is used to maintain the coordinates of the sound
	with a reference to the listener also being maintained.

	Also been modified to have two different playing states:
	The first is the original state used in My3DSound in which the sound would be looped continuously
		this is the default state but can also be invoked by calling SetInterval() with both parameters equal to 0.0f
	The second is a random delay state where the sound will be played periodically after a set amount of time,
		this state can be invoked by calling the SetInterval() function with both parameters not equal to 0.0f
		Both values can be set the the same which will result in the intervals being constant

*/

#ifndef _3DSound_h__
#define _3DSound_h__

// system includes.
#include <Windows.h>
#include <XAudio2.h>
#include <X3DAudio.h>
#include <memory>
#include <string>

// Framework includes.
#include "Updateable.hpp"

//--- forward declarations.
namespace AllanMilne { namespace Audio { class XASound; } }

class C3DSound : public AllanMilne::Updateable {
public:
	//--- constructor: creates XASound object for supplied .wav file at supplied position.
	C3DSound (const std::string& aWavFile, const float x, const float y, const float z, X3DAUDIO_LISTENER* aListener); 
	
	//--- Destructor: stop sound playing; no deletion required since smart pointers used.
	virtual ~C3DSound();

	//=== Implementation of the Updateable interface.

	//--- when activated the sound should begin playing.
	virtual void OnUpdateActivation ();
	//--- When deactivated the sound should stop playing.
	virtual void OnUpdateDeactivation (); 
	//--- Called every frame to recalculate and set DSP settings for the sound position; deltaTime not used.
	virtual void OnUpdate (const float deltaTime);

	//=== Class specific behaviour.

	//--- Indicate if a valid object has been created.
	inline bool IsValid() const { return m_sound->IsValid(); }

	//--- return reference to the XASound object encapsulating the underlying sound element.
	inline std::shared_ptr<AllanMilne::Audio::XASound> GetXASound() const { return m_sound; }

	//--- getter and setter for the sound position.
	inline X3DAUDIO_VECTOR GetPosition () const { return m_emitter.Position; }
	void SetPosition (const float x, const float y, const float z);

	//--- Allow the listener to be changed.
	inline void SetListener (X3DAUDIO_LISTENER* aListener) { m_listener = aListener; }
	//--- provide access to the emitter struct
	inline X3DAUDIO_EMITTER* GetEmitter() { return &m_emitter; }

	//--- instead of looping the sound, cause it to be played randomly between two intervals
	//--- Set both values to 0.0f to just loop normally
	void SetInterval(float min, float max);

protected:
	//--- the sound element to be played.
	std::shared_ptr<AllanMilne::Audio::XASound> m_sound;

	//--- the emitter defining position of the sound element.
	X3DAUDIO_EMITTER m_emitter;

	//--- A pointer to the listener used for this sound element.
	X3DAUDIO_LISTENER* m_listener;

	//--- random interval variables, both in seconds
	float m_randomMin;
	float m_randomMax;
	//--- the time since the sound was last played, only set if m_randomIntervalsEnabled is true
	float m_timeSinceLastPlayed;
	//--- the value m_timeSinceLastPlayed must reach before the sound is played again
	float m_nextPlayed;
	
	bool m_randomIntervalsEnabled;

}; // end C3DSound class.

#endif //_3DSound_h__