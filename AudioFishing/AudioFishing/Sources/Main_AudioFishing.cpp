/*
	File:	Main_AudioFishing.cpp
	Version:	1.0
	Date: 3rd April 2014.
	Author:	Adam Docherty.

	Uses:	WinCore, IState, XACore, FishingGame.

	Description:
	Modified from Main_Walking.cpp v1.0 as supplied in the solution to the 
	Soundscape3 practical by Allan C. Milne.

	A main code file containing the Windows application entry point

	Modified to add console output functionality for debugging

*/

//--- system includes.
#include <windows.h>
#include <memory>
#ifdef _DEBUG
#include <stdio.h>
#include <fcntl.h>
#include <io.h>
#include <iostream>
#include <fstream>
#endif//_DEBUG

//--- framework includes.
#include "WinCore.hpp"
#include "IState.hpp"
#include "XACore.hpp"
#include "waveFileManager.hpp"
using namespace AllanMilne;
using namespace AllanMilne::Audio;

//--- Application specific include.
#include "FishingGame.hpp"

namespace {

#ifdef _DEBUG

static const WORD c_maxConsoleLines = 512;

void RedirectIOToConsole()
{
	int consoleHandle;
	long stdHandle;
	CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
	FILE *fp;

	// allocate a console for this app
	AllocConsole();
	// set the screen buffer to be big enough to let us scroll text
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &consoleInfo);
	consoleInfo.dwSize.Y = c_maxConsoleLines;
	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), consoleInfo.dwSize);
	
	// redirect unbuffered STDOUT to the console
	stdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
	consoleHandle = _open_osfhandle(stdHandle, _O_TEXT);
	fp = _fdopen( consoleHandle, "w" );
	*stdout = *fp;
	setvbuf( stdout, NULL, _IONBF, 0 );
	// redirect unbuffered STDIN to the console
	stdHandle = (long)GetStdHandle(STD_INPUT_HANDLE);
	consoleHandle = _open_osfhandle(stdHandle, _O_TEXT);
	fp = _fdopen( consoleHandle, "r" );
	*stdin = *fp;
	setvbuf ( stdin, NULL, _IONBF, 0 );
	// redirect unbuffered STDERR to the console
	stdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);
	consoleHandle = _open_osfhandle(stdHandle, _O_TEXT);
	fp = _fdopen( consoleHandle, "w" );
	*stderr = *fp;
	setvbuf( stderr, NULL, _IONBF, 0 );
	// make cout, wcout, cin, wcin, cerr, wcerr, clog, and wclog
	// point to the console as well
	std::ios::sync_with_stdio();

}

#endif// _DEBUG
}

//=== Application entry point. ===
int WINAPI WinMain (HINSTANCE hinstance,
				   HINSTANCE prevInstance, 
				   PSTR cmdLine,
				   int showCmd)
{

#ifdef _DEBUG
	RedirectIOToConsole();
#endif// _DEBUG

	std::unique_ptr<WinCore> windowApp;
	std::unique_ptr<IState> frameProcessor;
	
	//--- Create the singleton XACore objec that will initialize XAudio2 and X3DAudio.
	//--- Must do this before creating/initializing other objects since these may require XAudio2 functionality in creating sounds, etc.
	XACore::CreateInstance();
	if (XACore::GetStatus() != XACore::OK) {
		MessageBox (NULL, TEXT ("Error initializing XAudio2 - application aborted."), TEXT ("Main Application"), MB_OK | MB_ICONERROR );
		return 0;
	}

	//--- create the object that encapsulates frame processing.
	frameProcessor.reset(new FishingGame());

	//--- Create the WinCore object composed with the frame processor.
	windowApp.reset(new WinCore (frameProcessor.get()));

	//--- Initialize the WinCore object including creating the application window;
	//--- this will also call the IState::Initialize() method of the frame processor.
	bool ok = windowApp->Initialize (
				" Fishing Game ",		// Windows title bar text.
				800, 600,			// width x height
				false,				// use full screen; change to true if windowed is required.
				hinstance );
	if (!ok) {
		MessageBox (NULL, TEXT ("Error occurred while initializing WinCore; application aborted."), TEXT (" Main Application"), MB_OK | MB_ICONERROR );
		return 0;
	}

	//--- Run the application Windows messsage loop and associated frame processing.
	windowApp->RunApp();

	//--- Delete resources.
	//--- NB order of deletion is important - is in opposite order of creation.

	//--- delete the WinCore instance explicitly before we delete other resources.
	windowApp.release();
	
	//--- as above, delete the IState object.
	frameProcessor.release();
	
	//--- Delete the XACore singleton instance - will clear up all XAudio2 resources.
	XACore::DeleteInstance();
	
	//--- Delete the WaveFileManager Instance to release all PCMWave objects that might have been created.
	WaveFileManager::DeleteInstance();

	return 0;
} // end WinMain function.

// end of code.