#include "XASound.hpp"

#include "Bird.h"

namespace {
	const std::string c_birdSoundFilename = "Sounds/bird.wav";
}

CBird::CBird( const float x, const float y, const float z, X3DAUDIO_LISTENER* aListener, 
	float radius, float speed )
	: C3DSound( c_birdSoundFilename, x, y, z, aListener )
	, m_radius(radius)
	, m_speed(speed)
	, m_angle(0.0f)
{
	m_centre.x = x;
	m_centre.y = y;
	m_centre.z = z;

	m_emitter.CurveDistanceScaler = 5.0f;
}

CBird::~CBird()
{
}

void CBird::OnUpdateActivation()
{
	return C3DSound::OnUpdateActivation();
}

void CBird::OnUpdateDeactivation()
{
	return C3DSound::OnUpdateDeactivation();
}

void CBird::OnUpdate( const float deltaTime )
{
	// the amount the angle is incremented by per second
	const float t = m_speed / m_radius;
	m_angle += t * deltaTime;
	while (m_angle > X3DAUDIO_2PI) {
		m_angle -= X3DAUDIO_2PI;
	}
	const float x = m_radius*cos(m_angle) + m_centre.x;
	const float z = m_radius*sin(m_angle) + m_centre.z;

	SetPosition(x, m_centre.y, z);
	//SetPosition(0.0f, 0.0f, 0.0f);
	//m_sound->Play();

	return C3DSound::OnUpdate( deltaTime );
}