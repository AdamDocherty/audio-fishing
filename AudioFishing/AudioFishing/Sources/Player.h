/*
	File:	Player.h
	Date:	5th April 2014.
	Author:	Adam Docherty.

	Requires: XASound, Updateable, Bobber
	
	Description:
	Encapsulation of the player entity, can be moved up and down the river
	with the MoveUpRiver() and MoveDownRIver() functions, the locations are
	defined as enumerations in the CPlayer namespace. 

	They player owns a CBobber object which represents the fishing line.

	The player contains the X3DAUDIO_LISTENER struct that all other sounds in the
	scene should calculate their 3D audio with.

	The player inherits from the AllanMilne::Updatable class and should be updated
	every frame.

*/

#ifndef Player_h__
#define Player_h__

// system includes.
#include <Windows.h>
#include <XAudio2.h>
#include <X3DAudio.h>
#include <memory>
#include <string>

// Framework includes.
#include "Updateable.hpp"

//--- forward declarations.
namespace AllanMilne { namespace Audio { class XASound; } }
class CBobber;

class CPlayer : public AllanMilne::Updateable 
{
public:
	enum Location {
		// the location 0 is the top of the river
		LOCATION_NEAR_WATERFALL = 0,
		LOCATION_MIDDLE_OF_RIVER, // behind the rock, defalt starting position
		LOCATION_BOTTOM_OF_RIVER,
		LOCATION_count
	};

	CPlayer();
	~CPlayer() {}

	//--- casts the line
	void Cast();
	//--- reels in the line
	void ReelIn();
	//--- yanks the rod left
	void YankLeft();
	//--- yanks the rod right
	void YankRight();
	//--- move up the river
	void MoveUpRiver();
	//--- move down the river
	void MoveDownRiver();

	//--- Implementation of Updateable's OnUpdate function
	virtual void OnUpdate( const float deltaTime );

	//--- getter and setter for the current position of the player
	inline const X3DAUDIO_VECTOR& GetPosition() const { return m_listener.Position; }
	void SetPosition(float x, float y, float z);

	//--- provides access to the listener
	inline X3DAUDIO_LISTENER* GetListener() { return &m_listener; }

protected:
	//--- returns true if the player is currently fishing
	bool isFishing() const;
	//--- Returns true if the player is currently moving
	bool isMoving() const;

	//--- moves towards the target location
	void Move( const float deltaTime );
	//--- update the direction the player is facing
	void UpdateOrientation( const X3DAUDIO_VECTOR& direction, const float deltaTime );

	//--- called when the player reaches their location and stops moving
	void OnLocationReached();
	//--- called when the player has a new location and starts moving
	void OnStartMoving();

	//--- The bobber of the rod attached to the player
	std::shared_ptr<CBobber> m_bobber;
	//--- the sounds of the footsteps, played when moving to a new location
	std::shared_ptr<AllanMilne::Audio::XASound> m_footstepsSound;
	//--- The listener
	X3DAUDIO_LISTENER m_listener;

	//--- the speed the player is reeling in the line
	float m_reelInSpeed;
	//--- the speed at which the player moves to new locations
	float m_movementSpeed;
	//--- the current location of the player, or the location the player is currently moving to
	int m_location;
};

#endif // Player_h__
