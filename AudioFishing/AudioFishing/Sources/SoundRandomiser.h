/*
	File:	Bobber.h
	Date:	5th April 2014.
	Author:	Adam Docherty.

	Modification of the XASound class provided in AllanMilne's framework

	Requires: ISound, PCMWave
	
	Description:
	This class models multiple sounds that are played randomly through the same XAudio2 source voice with an associated sound buffer.
	It exposes basic playing behaviour is defined through the ISound interface
	and extends this with XAudio2 specific behaviour.
	* See ISound.hpp for the basic behaviour.
	* note that volume units are in dB in this implementation.

*/

#ifndef SoundRandomiser_h__
#define SoundRandomiser_h__

// system includes
#include <XAudio2.h>
#include <string>
#include <vector>

// framework include
#include "ISound.hpp"

namespace AllanMilne { namespace Audio { class PCMWave; } }

class CSoundRandomiser : public AllanMilne::Audio::ISound
{
public:
	CSoundRandomiser();
	CSoundRandomiser(const std::string& filename);

	//--- Add a new sound effect to the list of randomly playing sounds
	void AddSound(const std::string& filename);

	//--- add a new sound to the queue if looping is enabled
	void Update();

	//--- Query if sound is valid; has a sourcevoice that can play back audio
	bool IsValid() const { return GetSourceVoice() != nullptr; }

	//--- Access to the source voice to allow other methods to be called.
	inline IXAudio2SourceVoice* GetSourceVoice () const { return m_sourceVoice; }

	//--- access to the XAudio2 buffer array
	inline std::vector<XAUDIO2_BUFFER>& GetBufferArray () { return m_XABufferArray; }

	//--- Route this sound through the specified submix voice.
	void RouteToSubmixVoice (IXAudio2SubmixVoice* aSubmixVoice);

	//--- Panning - is only applied if we have stereo device channels; assumes output channels = device channels.
	//--- Works best for mono sources - multiple channels will be merged.
	//--- Value is between -1 and 1;
	//--- 0 = middle; -1 = far left; 1 = far right.
	//--- setting values outside this range has no effect.
	inline virtual float GetPan () const { return m_pan; }
	virtual void SetPan (const float aPan);
	virtual void AdjustPan (const float anAmount);

	//--- Pitch control; values are in semitones.
	//--- this adjusts the frequency ratio of the source voice; if you want to use the native frequency ratio then access the source voice directly.
	float GetPitch () const;
	virtual void SetPitch (const float aPitch);
	virtual void AdjustPitch (const float anAmount);

	//--- Filtering behaviour; if source voice has not been enabled with filtering then these functions have no effect.
	//--- cut-off frequency is in Hz; if <0 then set to 0.
	//--- 1/Q value is between 0 and 1.0; if outside range then set to 0 or 1 respectively.
	bool IsFiltered () const;
	float GetFilterCutoffFrequency () const;
	virtual void SetFilterCutoffFrequency (const float aFrequency);
	virtual void AdjustFilterCutoffFrequency (const float anAmount);
	float GetFilter1OverQ () const;
	virtual void SetFilter1OverQ (const float a1OverQ);
	virtual void AdjustFilter1OverQ (const float anAmount);
	XAUDIO2_FILTER_TYPE GetFilterType () const;
	virtual void SetFilterType (const XAUDIO2_FILTER_TYPE aType);
	virtual void SetFilter (const XAUDIO2_FILTER_TYPE aType, const float aFrequency, const float a1OverQ=1.0f);
	// Accessing the underlying XAudio2 voice's filter parameters struct.
	XAUDIO2_FILTER_PARAMETERS GetFilterParameters () const;
	virtual void SetFilterParameters (XAUDIO2_FILTER_PARAMETERS &aParameters);

	//=== Implementation of ISound interface

	void Play( int aFlags=0 );
	bool IsPlaying() const;

	void Stop();

	void Pause();
	inline bool IsPaused() const { return m_isPaused; }

	void Unpause();
	void TogglePause();

	void SetLooped( const bool aLooped );
	bool IsLooped() const;

	float GetVolume() const;
	void SetVolume( const float aVolume );
	void AdjustVolume( const float anIncrement );

protected:

	IXAudio2SourceVoice *m_sourceVoice;
	std::vector<XAUDIO2_BUFFER>	m_XABufferArray;
	bool				m_isPaused;
	bool m_looped;
	float m_pan;
	int m_flags;
};

#endif // SoundRandomiser_h__
