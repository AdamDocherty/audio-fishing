#include "MusicTrack.h"


// framework includes.
#include "XACore.hpp"
#include "WaveFileManager.hpp"
#include "PCMWave.hpp"

namespace {
	using namespace AllanMilne::Audio;
	
	//--- Initialise source voice and buffer from a PCMWave object; called from constructors.
	void InitXASound (PCMWave * aWave, bool filtered, IXAudio2SourceVoice **aVoice, XAUDIO2_BUFFER *aBuffer)
	{
		// guard against invalid wave format.
		if (aWave->GetStatus() != PCMWave::OK)  return;

		// copy windows wave format struct from the PCMWave struct field.
		WAVEFORMATEX wFmt;
		memcpy_s (&wFmt, sizeof (WaveFmt), &(aWave->GetWaveFormat()), sizeof (WaveFmt));
		// Create the source voice for the specified wave format; return if failed.
		// Source voice will be routed directly to the mastering voice since no target voice is specified.
		unsigned int flag = (filtered) ? XAUDIO2_VOICE_USEFILTER : 0;
		HRESULT hr = XACore::GetInstance()->GetEngine()->CreateSourceVoice (aVoice, &wFmt, flag );
		if( FAILED( hr ) ) return;

		// Create and initialise the XAudio2 buffer struct from the PCMWave object.
		ZeroMemory (aBuffer, sizeof(XAUDIO2_BUFFER));
		aBuffer->AudioBytes = aWave->GetDataSize ();
		aBuffer->pAudioData = (BYTE*)(aWave->GetWaveData ());

		/*--- Following listed here for reference - have already been set to 0 via ZeroMemory above.
		aBuffer->Flags = 0;			// almost always 0; =XAUDIO2_END_OF_STREAM to suppress some debug warnings re buffer starvation.
		aBuffer->PlayBegin = 0;		// First sample in the buffer that should be played.
		aBuffer->PlayLength = 0;		// Number of samples to play; 0=entire buffer (begin must also be 0).
		aBuffer->LoopBegin = 0;		// First sample to be looped; must be <(PlayBegin+PlayLength); can be <PlayBegin.
		aBuffer->LoopLength = 0;		// Number of samples in loop; =0 indicates entire sample; PlayBegin > (LoopBegin+LoopLength) < PlayBegin+PlayLength).
		aBuffer->LoopCount = 0;		// Number of times to loop; =XAUDIO2_LOOP_INFINITE to loop forever; if 0 then LoopBegin and LoopLength must be 0.
		aBuffer->pContext = NULL;		// context to be passed to the client in callbacks.
		---*/
	} // end InitXASound function.
}



CMusicTrack::CMusicTrack( const std::string& filename, UINT32 loopBegin /*= 0*/, UINT32 loopLength /*= 0*/ )
	: m_sourceVoice(nullptr)
	, m_paused(false)
{
	const bool filtered = false;

	PCMWave *waveBuffer = WaveFileManager::GetInstance().LoadWave (filename);
	InitXASound (waveBuffer, filtered, &m_sourceVoice, &m_XABuffer);
	m_XABuffer.LoopBegin = loopBegin;
	m_XABuffer.LoopLength = loopLength;
	m_XABuffer.LoopCount = XAUDIO2_LOOP_INFINITE;
}

CMusicTrack::~CMusicTrack()
{
	if (m_sourceVoice != NULL) {
		m_sourceVoice->Stop();
		m_sourceVoice->FlushSourceBuffers();
		m_sourceVoice->DestroyVoice();
	}
}

void CMusicTrack::Play()
{
	if (IsPlaying()) { return; }
	if (m_paused)
	{	// reset buffer.
		m_sourceVoice->FlushSourceBuffers();
		m_paused = false;
	}
	m_sourceVoice->SubmitSourceBuffer (&m_XABuffer);
	m_sourceVoice->Start (0, XAUDIO2_COMMIT_NOW);
}

void CMusicTrack::Stop()
{
	if (IsPlaying() || m_paused)
	{
		m_sourceVoice->Stop();
		m_sourceVoice->FlushSourceBuffers();
		m_paused = false;
	}
}

bool CMusicTrack::IsPlaying()
{
	if (m_paused) { return false; }
	XAUDIO2_VOICE_STATE vState;
	m_sourceVoice->GetState (&vState);
	return ( vState.BuffersQueued > 0 );
}

void CMusicTrack::SetVolume( float volume )
{
	float ampRatio = XAudio2DecibelsToAmplitudeRatio (volume);
	// Guard against to large or small values.
	if (ampRatio<-XAUDIO2_MAX_VOLUME_LEVEL || ampRatio>XAUDIO2_MAX_VOLUME_LEVEL) { return; }
	m_sourceVoice->SetVolume (ampRatio);
}
