/*
	File:	XAMusicManager.h
	Date:	23rd April 2014.
	Author:	Adam Docherty.

	Requires: MusicTrack

	Description:
	Implementation of the IMusicManager interface using XAudio2

	Music tracks will crossfade when calling Play() when another track is currently
	playing.

	Music tracks must be loaded in when XAMusicManager is created using the
	AddTrack() method. 

*/

#ifndef XAMusicManager_h__
#define XAMusicManager_h__

#include <Windows.h>
#include <XAudio2.h>

#include "IMusicManager.h"

#include <map>

class CMusicTrack;

class XAMusicManager : public IMusicManager
{
public:
	XAMusicManager();
	~XAMusicManager() {}

	//--- load a music track from a soundfile, loopBegin defines how many samples before looping begins
	//--- loopLength defines how many samples the track will loop for before returning to loopBegin
	void AddTrack( MusicTrackType trackId, const std::string& filename, UINT32 loopBegin, UINT32 loopLength );
	//=== IMusicManager implementation
	//--- change the currently playing track, or start playing a track if no track is playing
	//--- will have a short crossfade in between tracks
	virtual void PlayTrack( MusicTrackType trackId );
	//--- set the volume
	virtual void SetVolume( float volume );
	//--- stop the currently playing track
	virtual void Stop(); 
	//--- update method required for crossfade
	virtual void Update( float deltaTime );
	//--- toggle mute on and off
	virtual void ToggleMute( );

private:
	//--- returns the default volume unless the music manager is muted, in which case returns silence
	inline float CurrentVolume() { return m_muted ? -144.5f : m_defaultVolume; }
	//--- collection which stores all loaded music tracks
	std::map<MusicTrackType, std::shared_ptr<CMusicTrack>> m_trackMap;
	//--- the current track
	std::shared_ptr<CMusicTrack> m_currentTrack;
	//--- the previous track, used for crossfading
	std::shared_ptr<CMusicTrack> m_previousTrack;

	//--- the volume of the music when unmuted
	float m_defaultVolume;
	//--- the amount of seconds for the tracks to transition
	float m_fadeTime;
	//--- the current progress of the track transition, reset to 0 whenever Play() is called
	float m_currentTime;

	bool m_muted;

};

#endif // XAMusicManager_h__
