/*
	File:	IMusicManager.h
	Date:	23rd April 2014.
	Author:	Adam Docherty.

	Description:
	An interface for Music Manager implementations.

	All sound tracks used have a corresponding MusicTrackType enumeration in
	the IMusicManager namespace, though the implementation for how the music
	tracks are loaded should be handled by the implementation classes

	The Music Manager for the application can be accessed using the Service 
	Locator class.

*/

#ifndef IMusicManager_h__
#define IMusicManager_h__

class IMusicManager
{
public:
	enum MusicTrackType {
		MUSIC_TRACK_DEFAULT = 0,
		MUSIC_TRACK_FISH_HOOKED,
		MUSIC_TRACK_count
	};

	virtual ~IMusicManager() = 0;
	//--- update method, should it be required by the implementation class
	virtual void Update(float deltaTime) = 0;
	//--- change the currently playing track, or start playing a track if no track is playing
	virtual void PlayTrack(MusicTrackType trackId) = 0;
	//--- set the volume
	virtual void SetVolume(float volume) = 0;
	//--- toggle mute on and off
	virtual void ToggleMute() = 0;
	//--- stop the currently playing track
	virtual void Stop() = 0;
};
inline IMusicManager::~IMusicManager() {}

#endif // IMusicManager_h__
