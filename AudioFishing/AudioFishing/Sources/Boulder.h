/*
	File:	Boulder.h
	Date:	30th April 2014.
	Author:	Adam Docherty.
	
	Description:
	Encapsulation of a spherical boulder which blocks sounds coming from behind it.
	
	Any audio emitters that are blocked by the boulder should call 
	the calculateEmitterCone() with itself as an argument.

	Only one boulder can be in the scene at a time for this to work. For multiple boulders
	more complex methods of generating the sound cones must be used.

*/

#ifndef Boulder_h__
#define Boulder_h__

// system includes.
#include <Windows.h>
#include <XAudio2.h>
#include <X3DAudio.h>

class CBoulder {
public:
	CBoulder(X3DAUDIO_VECTOR position, float radius);

	//--- calculate the pCone component of the emitter, none of the other members are modified, 
	void calculateEmitterCone(X3DAUDIO_EMITTER* emitter) const;
protected:
	float calculateTangentAngle( float length ) const;

	//--- the radius of the boulder
	float m_radius;
	//--- the position of the boulder
	X3DAUDIO_VECTOR m_position;
};




#endif // Boulder_h__