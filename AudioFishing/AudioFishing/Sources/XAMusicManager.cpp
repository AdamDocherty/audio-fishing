#include "XAMusicManager.h"

#ifdef _DEBUG
#include <iostream>
#endif// _DEBUG

#include "MusicTrack.h"

namespace {

	const float c_defaultVolume = -15.0f;
	const float c_defaultFadeTime = 1.5f;

}

XAMusicManager::XAMusicManager()
	: m_defaultVolume(c_defaultVolume)
	, m_fadeTime(c_defaultFadeTime)
	, m_currentTime(m_fadeTime)
	, m_muted(false)
{
}

void XAMusicManager::AddTrack( MusicTrackType trackId, const std::string& filename, UINT32 loopBegin, UINT32 loopLength )
{
	if (trackId < 0 || trackId >= IMusicManager::MUSIC_TRACK_count) return;
	if (m_trackMap.count(trackId) != 0) {
		m_trackMap.erase(trackId);
	}

	m_trackMap[trackId] = std::make_shared<CMusicTrack>(filename, loopBegin, loopLength);
}

void XAMusicManager::PlayTrack( MusicTrackType trackId )
{
	if (trackId < 0 || trackId >= IMusicManager::MUSIC_TRACK_count) return;
	if (m_trackMap.count(trackId) == 0) return;

	if (m_currentTrack.get() != nullptr) {
		m_currentTime = 0.0f;
		m_previousTrack = m_currentTrack;
		m_previousTrack->SetVolume(CurrentVolume());
	}
	m_currentTrack = m_trackMap[trackId];
	m_currentTrack->SetVolume(CurrentVolume());
	m_currentTrack->Play();

#ifdef _DEBUG
	std::cout << "Playing audio track " << trackId << "." << std::endl;
#endif// _DEBUG
}

void XAMusicManager::SetVolume( float volume )
{
	m_defaultVolume = volume;
}

void XAMusicManager::Stop()
{
	for (auto it = m_trackMap.begin(); it != m_trackMap.end(); ++it) {
		it->second->Stop();
	}
}

void XAMusicManager::Update( float deltaTime )
{
	if (m_currentTime < m_fadeTime) {
	m_currentTime += deltaTime;
		if (m_currentTime >= m_fadeTime) {
			m_previousTrack->Stop();
			m_currentTrack->SetVolume(CurrentVolume());
		} else if (!m_muted) {
			const float maxVolumeDb = 30.0f;

			float curTrackVolume = -maxVolumeDb + ((maxVolumeDb + m_defaultVolume) * (m_currentTime / m_fadeTime));
			float prevTrackVolume = -maxVolumeDb + ((maxVolumeDb + m_defaultVolume) * (1.0f - (m_currentTime / m_fadeTime)));
			m_currentTrack->SetVolume(curTrackVolume);
			m_previousTrack->SetVolume(prevTrackVolume);
		}
	}
}

void XAMusicManager::ToggleMute()
{
	m_muted = !m_muted;

	if (m_muted) {
		if (m_currentTrack.get() != nullptr) m_currentTrack->SetVolume(-144.5f);
		if (m_previousTrack.get() != nullptr) m_previousTrack->SetVolume(-144.5f);
	} else {
		m_currentTrack->SetVolume(m_defaultVolume);
		// deal with cases where the music is muted while the tracks are fading in/out
		Update(0.0f); 
	}
}
