#include "Boulder.h"
#include "StuVector3.hpp"
#include "StuMath.hpp"

namespace {

void InitialiseEmitter( X3DAUDIO_EMITTER* emitter ) 
{
	emitter->pCone = new X3DAUDIO_CONE;
	SecureZeroMemory(emitter->pCone, sizeof X3DAUDIO_CONE);
	emitter->pCone->InnerVolume = 0.2f;
	emitter->pCone->OuterVolume = 1.0f;
	emitter->pCone->InnerLPF = 1.0f;
	emitter->pCone->OuterLPF = 1.0f;
}

void SetEmitterOrient( X3DAUDIO_EMITTER* emitter, stu::v3f &dir )
{
	emitter->OrientFront.x = dir.x;
	emitter->OrientFront.y = dir.y;
	emitter->OrientFront.z = dir.z;
	// don't need to calculate orient top since it is not used in sound cone calculations
}

}

CBoulder::CBoulder( X3DAUDIO_VECTOR position, float radius )
	: m_position(position)
	, m_radius(radius)
{
}

void CBoulder::calculateEmitterCone( X3DAUDIO_EMITTER* emitter ) const
{
	const stu::v3f boulderCentre = { m_position.x , m_position.y, m_position.z };
	const stu::v3f emitterPos = { emitter->Position.x, emitter->Position.y, emitter->Position.z };
	stu::v3f dir =  boulderCentre - emitterPos;
	float length = dir.Magnitude();

	float tangentAngle = calculateTangentAngle(length);

	if (NULL == emitter->pCone) {
		InitialiseEmitter(emitter);
	}

	X3DAUDIO_CONE * cone = emitter->pCone;
	cone->InnerAngle = tangentAngle;
	cone->OuterAngle = tangentAngle;

	dir.Normalise();
	SetEmitterOrient(emitter, dir);
}

float CBoulder::calculateTangentAngle( float length ) const
{
	if (m_radius > length) {
		return X3DAUDIO_2PI;
	} else {
		return asinf(m_radius/length);
	}
}


