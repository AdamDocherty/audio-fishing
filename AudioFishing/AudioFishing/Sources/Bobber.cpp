#include "Bobber.h"

#include <iostream>

#include "XACore.hpp"
#include "XASound.hpp"
#include "SoundRandomiser.h"

#include "StuVector3.hpp"

#include "ServiceLocator.h"
#include "IMusicManager.h"

using namespace AllanMilne::Audio;

namespace {
	const std::string c_splashSoundFilename = "Sounds/bobberSplash.wav";
	const float c_splashSoundVolume = 10.0f;

	const std::string c_reelSoundFilename = "Sounds/fishingReel.wav";
	const float c_reelSoundVolume = -10.0f;

	const std::string c_caughtSoundFilename = "Sounds/fishCaught.wav";
	const std::string c_yankSoundFilename = "Sounds/yank.wav";
	const std::string c_snapSoundFilename = "Sounds/snap.wav";

	const unsigned int c_numFishSounds = 3;
	const std::string c_fishSoundFilenames[] = {
		"Sounds/fishSplashing_01.wav",
		"Sounds/fishSplashing_02.wav",
		"Sounds/fishSplashing_03.wav"
	};
	const float c_fishSoundVolume = 20.0f;

	//--- the factor that the distance between the player and bobber should be multiplied with to calculate the fight time
	const float c_flightTimeFactor = 0.1f;
	//--- the speed the fish will rotate around the player
	const float c_fishSwimSpeed = 2.0f;
	//--- the speed the fish gets dragged back to the middle when the rod is being yanked
	const float c_yankSpeed = 10.0f;
	//--- the angle at which the line will snap if the fish rotates beyond
	const float c_fishLimit = 10.0f; 
	//--- the default value for m_fishSwimDelay in seconds
	const float c_defaultFishSwimDelay = 1.0f;

	float randFloat(float lo, float hi) {
	    return lo + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(hi-lo))); 	
	}
}

CBobber::CBobber( const X3DAUDIO_LISTENER* listener )
	: m_listener(listener)
	, m_state(BOBBER_STATE_NOT_CAST)
	, m_fishTimer(0.0f)
	, m_fishSwimDelay(c_defaultFishSwimDelay)
{
	InitialiseSounds();

	// Set up the emitter struct.
	SecureZeroMemory (&m_emitter, sizeof X3DAUDIO_EMITTER);
	XAUDIO2_VOICE_DETAILS details;
	m_splashSound->GetSourceVoice()->GetVoiceDetails(&details);
	m_emitter.ChannelCount = details.InputChannels;
	m_emitter.CurveDistanceScaler = 4.0f;
}

void CBobber::InitialiseSounds()
{
	// Create the sound element.
	m_splashSound = std::make_shared<XASound>(c_splashSoundFilename, true);			// true= filters enabled.
	if (!m_splashSound->IsValid()) {
		return;		// Can do no more.
	}
	m_splashSound->SetVolume(c_splashSoundVolume);

	m_reelSound = std::make_shared<XASound>(c_reelSoundFilename, true);
	if (!m_reelSound->IsValid()) {
		return;
	}
	m_reelSound->SetLooped(true);
	m_reelSound->SetVolume(c_reelSoundVolume);

	m_fishSound = std::make_shared<CSoundRandomiser>();
	for (unsigned int i = 0; i < c_numFishSounds; ++i) {
		m_fishSound->AddSound(c_fishSoundFilenames[i]);
	}
	if (!m_splashSound->IsValid()) {
		return;
	}
	m_fishSound->SetLooped(true);
	m_fishSound->SetVolume(c_fishSoundVolume);

	m_caughtSound = std::make_shared<XASound>(c_caughtSoundFilename, true);
	if (!m_caughtSound->IsValid()) {
		return;
	}

	m_yankSound = std::make_shared<XASound>(c_yankSoundFilename, true);
	if (!m_yankSound->IsValid()) {
		return;
	}

	m_snapSound = std::make_shared<XASound>(c_snapSoundFilename, true);
	if (!m_snapSound->IsValid()) {
		return;
	}

}


void CBobber::OnUpdate( const float deltaTime )
{
	switch (m_state) {
	case BOBBER_STATE_NOT_CAST:
		break;
	case BOBBER_STATE_FLIGHT:
		UpdateFlight(deltaTime);
		break;
	case BOBBER_STATE_CAST:
		UpdateCast(deltaTime);
		break;
	case BOBBER_STATE_FISH_HOOKED:
		UpdateFishHooked(deltaTime);
		UpdateFish(deltaTime);
		break;
	}
}

void CBobber::UpdateFlight( const float deltaTime )
{
	m_flightTime -= deltaTime;

	if (m_flightTime <= 0.0f) {
		OnBobberLanded();
	}
}

void CBobber::UpdateFishHooked( const float deltaTime )
{
	ReelIn(deltaTime);
	m_fishSound->Update();
}

void CBobber::UpdateCast( const float deltaTime )
{
	ReelIn(deltaTime);

	m_waitingTime -= deltaTime;

	if (m_waitingTime <= 0.0f) {
		OnFishHooked();
	}
}

void CBobber::UpdateFish( const float deltaTime )
{
	switch(m_fishState) {
	case FISH_STATE_NO_FISH:
		break; // fish has just been reeled in
	case FISH_STATE_STILL:
		m_fishTimer += deltaTime;
		if (m_fishTimer > m_fishSwimDelay) {
			if (rand()%2 == 0) {
				m_fishState = FISH_STATE_SWIM_LEFT;
			} else {
				m_fishState = FISH_STATE_SWIM_RIGHT;
			}
		}
		break;
	case FISH_STATE_SWIM_LEFT:
		MoveFish(- c_fishSwimSpeed * deltaTime);
		// if fish is too far left
		if (GetDeltaX() < - c_fishLimit) {
			OnFishGotAway();
		}
		break;
	case FISH_STATE_SWIM_RIGHT:
		MoveFish(c_fishSwimSpeed * deltaTime);
		// if fish is too far right
		if (GetDeltaX() > c_fishLimit) {
			OnFishGotAway();
		}
		break;
	case FISH_STATE_YANK_LEFT:
		MoveFish(- c_yankSpeed * deltaTime);
		// once the fish is back in the middle
		if (GetDeltaX() < 0.0f) {
			m_fishState = FISH_STATE_STILL;
			m_fishTimer = 0.0f;
		}
		break;
	case FISH_STATE_YANK_RIGHT:
		MoveFish(c_yankSpeed * deltaTime);
		// once the fish is back in the middle
		if (GetDeltaX() > 0.0f) {
			m_fishState = FISH_STATE_STILL;
			m_fishTimer = 0.0f;
		}
		break;
	default:
#ifdef _DEBUG
		std::cout << "m_fishState is invalid: " << m_fishState << std::endl;
#endif//_DEBUG
		return;
	}
}

void CBobber::MoveFish(const float distance)
{
	GetPosition().x += distance;
}

void CBobber::SetPosition(float x, float y, float z)
{
	m_emitter.Position.x = x;
	m_emitter.Position.y = y;
	m_emitter.Position.z = z;
}

void CBobber::SetReelInSpeed(float speed)
{
	m_reelInSpeed = speed;
}

void CBobber::Cast( const X3DAUDIO_VECTOR& position )
{
	// return if the bobber has already been cast
	if (m_state != BOBBER_STATE_NOT_CAST) return;

	m_state = BOBBER_STATE_FLIGHT;

	SetPosition(position.x, position.y, position.z);

	CalculateFlightTime();

#ifdef _DEBUG
	std::cout << "Bobber being cast to (" << position.x << ", " << position.y << ", " << position.z << 
	"). With the flight time " << m_flightTime << "s" << std::endl;
#endif//_DEBUG
}

void CBobber::ReelIn( const float deltaTime )
{
	// reel in direction must be recalculated in case the fish has moved
	CalculateReelInDirection();
	X3DAUDIO_VECTOR newPosition = GetPosition();
	newPosition.x -= m_reelInDirection.x * m_reelInSpeed * deltaTime;
	newPosition.z -= m_reelInDirection.z * m_reelInSpeed * deltaTime;

	if (newPosition.z < GetPlayerPosition().z + 0.5f) {
		OnReeledIn();
		return;
	}

	SetPosition(newPosition.x, newPosition.y, newPosition.z);

	// Play the reeling sound
	if (m_reelInSpeed > 0.0f) {
		m_reelSound->SetPitch((m_reelInSpeed)/6.0f);
		m_reelSound->Unpause();
	} else {
		m_reelSound->Pause();
	}

	// Apply 3D audio to the fish splashing sound
	XACore::GetInstance()->Apply3D ( m_fishSound->GetSourceVoice(), &m_emitter, m_listener, 
	    X3DAUDIO_CALCULATE_MATRIX | X3DAUDIO_CALCULATE_LPF_DIRECT);
}

void CBobber::YankLeft()
{
	if (m_fishState == FISH_STATE_SWIM_RIGHT) {
		m_fishState = FISH_STATE_YANK_LEFT;
		OnYanked();
#ifdef _DEBUG
		std::cout << "Yanked left." << std::endl;
#endif//_DEBUG
	}
}

void CBobber::YankRight()
{
	if (m_fishState == FISH_STATE_SWIM_LEFT) {
		m_fishState = FISH_STATE_YANK_RIGHT;
		OnYanked();
#ifdef _DEBUG
		std::cout << "Yanked right." << std::endl;
#endif//_DEBUG
	}
}

void CBobber::CalculateReelInDirection()
{
	// Calculate the direction the bobber will be reeled in
	const stu::v3f bobberPosition= { GetPosition().x, 0.0f, GetPosition().z };
	const stu::v3f playerPosition= { GetPlayerPosition().x, 0.0f, GetPlayerPosition().z };

	stu::v3f dir = bobberPosition- playerPosition;
	dir.Normalise();

	m_reelInDirection.x = dir.x;
	m_reelInDirection.y = 0.0f;
	m_reelInDirection.z = dir.z;
}

void CBobber::CalculateFlightTime()
{
	m_flightTime = GetDistance() * c_flightTimeFactor;
}

void CBobber::OnReeledIn()
{
	if (m_state == BOBBER_STATE_FISH_HOOKED) {
		OnFishCaught();
	}

	m_reelSound->Stop();
}

void CBobber::OnBobberLanded()
{
	m_state = BOBBER_STATE_CAST;

	CalculateReelInDirection();

	XACore::GetInstance()->Apply3D ( m_splashSound->GetSourceVoice(), &m_emitter, m_listener, 
	    X3DAUDIO_CALCULATE_MATRIX | X3DAUDIO_CALCULATE_LPF_DIRECT);
	m_splashSound->Play();

	m_reelSound->Play();
	m_reelSound->Pause();

	m_waitingTime = randFloat(2.0f, 5.0f);
}

void CBobber::OnYanked()
{
	m_yankSound->Play();
}

void CBobber::OnFishHooked()
{
	m_state = BOBBER_STATE_FISH_HOOKED;
	m_fishSound->Play();
	CServiceLocator::getMusicManager()->PlayTrack(IMusicManager::MUSIC_TRACK_FISH_HOOKED);
	m_fishState = FISH_STATE_STILL;
}

void CBobber::OnFishUnhooked()
{
	CServiceLocator::getMusicManager()->PlayTrack(IMusicManager::MUSIC_TRACK_DEFAULT);
	m_fishSound->Stop();
	m_fishState = FISH_STATE_NO_FISH;
	m_state = BOBBER_STATE_NOT_CAST;
}

void CBobber::OnFishGotAway()
{
	OnFishUnhooked();
	m_snapSound->Play();
}

void CBobber::OnFishCaught()
{
	OnFishUnhooked();
	m_caughtSound->Play();
}

const X3DAUDIO_VECTOR& CBobber::GetPlayerPosition() const
{
	return m_listener->Position;
}

float CBobber::GetCurrentAngle() const
{
	float deltaZ = GetPosition().z - GetPlayerPosition().z;
	float deltaX = GetPosition().x - GetPlayerPosition().x;

	return atan2f(deltaZ, deltaX);
}

float CBobber::GetDistance() const
{
	stu::v3f pos = { GetPosition().x, GetPosition().y, GetPosition().z };
	stu::v3f playerPos = { GetPlayerPosition().x, 0.0f, GetPlayerPosition().z };

	return stu::v3f::Distance(pos, playerPos);
}

float CBobber::GetDeltaX() const
{
	return GetPosition().x - GetPlayerPosition().x;
}
