#include "ServiceLocator.h"

std::shared_ptr<IMusicManager> CServiceLocator::s_musicManagerService;

std::shared_ptr<IMusicManager> CServiceLocator::getMusicManager()
{
	return s_musicManagerService;
}

void CServiceLocator::provide( std::shared_ptr<IMusicManager> service )
{
	s_musicManagerService = service;
}

void CServiceLocator::clearMusicManager()
{
	s_musicManagerService.reset();
}
