#ifndef Bird_h__
#define Bird_h__

#include "3DSound.hpp"

class CBird : public C3DSound
{
public:
	//--- constructor: creates XASound object for supplied .wav file at supplied position.
	CBird (const float x, const float y, const float z, X3DAUDIO_LISTENER* aListener,
		float radius, float speed); 

	//--- Destructor: stop sound playing; no deletion required since smart pointers used.
	virtual ~CBird();

	//=== Implementation of the Updateable interface.

	//--- when activated the sound should begin playing.
	virtual void OnUpdateActivation ();
	//--- When deactivated the sound should stop playing.
	virtual void OnUpdateDeactivation (); 
	//--- Called every frame to recalculate and set DSP settings for the sound position; deltaTime not used.
	virtual void OnUpdate (const float deltaTime);

private:

	X3DAUDIO_VECTOR m_centre;
	float m_radius;
	float m_angle;
	float m_speed;

};



#endif // Bird_h__
