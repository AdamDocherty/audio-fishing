/*
	File:	FishingGame.cpp
	Version:	1.1
	Date:	18th February 2014.
	Author:	Allan C. Milne.

	Exposes:	implementation of the FishingGame class.
	Requires:	XASound, Updateable, C3DSound.
	
	Description:
	This is a solution to the Soundscape3 practical;
	
	This is the implementation of the class members;
	*	see FishingGame.hpp for the class definition and further information on the application functionality.

*/

// system includes.
#include <windows.h>
#include <xaudio2.h>
#include <X3DAudio.h>
#include <memory>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <math.h>

// framework includes.
#include "XACore.hpp"
#include "XASound.hpp"
#include "Updateable.hpp"
using namespace AllanMilne::Audio;
using AllanMilne::Updateable;

// application includes.
#include "FishingGame.hpp"
#include "3DSound.hpp"
#include "XAMusicManager.h"
#include "ServiceLocator.h"
#include "Boulder.h"
#include "Bird.h"

#include <map>
#include <string>


//=== Anonymous namespace defining constants and helper functions used in the implementation.
namespace 
{
	//--- the text file containing the 3d sounds information
	const std::string c_3dSoundsInfoFilename = "3dSounds.txt";

	//--- Distance for a single step.
	const float c_stepSize= 1.0f;

	//--- inital parameters for the position and radius of the boulder
	const X3DAUDIO_VECTOR c_boulderPos = { -10.0f, 0.0f, 2.5f };
	const float c_boulderRadius = 1.5f;

	//--- the location the river sound appears to originate from relative to the player
	const X3DAUDIO_VECTOR c_riverSoundPosition = { 0.0f, 0.0f, 5.0f };
	
	//--- Display instructions on using this application.
	void Instructions () 
	{
		using namespace std;
		stringstream msg;
		msg<<endl <<"You can walk through this soundscape using the following controls: " <<endl <<endl;
		msg <<" up arrow \t = move up the river." <<endl;
		msg <<" down arrow \t = move down the river. " <<endl;
		msg <<" 'f' \t = cast your fishing line. " <<endl;
		msg <<" 'r' \t = reel in your fishing line. " <<endl;
		msg <<" 'm' \t = mute the music. " << endl;
		msg <<" 'i' \t = display these instructions. " <<endl;
		msg <<" escape \t = end the application. " <<endl;
		const string msgStr = msg.str();
		MessageBox (NULL, msgStr.c_str(), TEXT("Fishing Instructions"), MB_OK | MB_ICONINFORMATION);
	} // end Instructions function.

	enum STRCONVERSION_ERROR { STRCON_SUCCESS = 0, 
		STRCON_OVERFLOW, 
		STRCON_UNDERFLOW, 
		STRCON_INCONVERTIBLE 
	};

	STRCONVERSION_ERROR str2int (int &i, char const *s, int base = 0)
	{
		char *end;
		long  l;
		errno = 0;
		l = strtol(s, &end, base);
		if ((errno == ERANGE && l == LONG_MAX) || l > INT_MAX) {
			return STRCON_OVERFLOW;
		}
		if ((errno == ERANGE && l == LONG_MIN) || l < INT_MIN) {
			return STRCON_UNDERFLOW;
		}
		if (*s == '\0' || *end != '\0') {
			return STRCON_INCONVERTIBLE;
		}
		i = l;
		return STRCON_SUCCESS;
	}

	STRCONVERSION_ERROR str2float (float &f, char const *s)
	{
		char *end;
		double d;
		errno = 0;
		d = strtod(s, &end);
		if ((errno == ERANGE && d == DBL_MAX) || d > FLT_MAX) {
			return STRCON_OVERFLOW;
		}
		if ((errno == ERANGE && d == - DBL_MAX) || d < - FLT_MAX) {
			return STRCON_UNDERFLOW;
		}
		if (*s == '\0' || *end != '\0') {
			return STRCON_INCONVERTIBLE;
		}
		f = static_cast<float>(d);
		return STRCON_SUCCESS;
	}

	bool LoadPositionFromBuffer( char * buffer, float& x, float& y, float& z )
	{
		char* context;
		const char* delims = " ,";
		const char *xstr = strtok_s(buffer, delims, &context);
		const char *ystr = strtok_s(NULL, delims, &context);
		const char *zstr = strtok_s(NULL, delims, &context);
		STRCONVERSION_ERROR res = STRCON_SUCCESS;
		res = str2float(x, xstr);
		if (res != STRCON_SUCCESS) return false;
		res = str2float(y, ystr);
		if (res != STRCON_SUCCESS) return false;
		res = str2float(z, zstr);
		if (res != STRCON_SUCCESS) return false;
		return true;
	}

	bool LoadMinAndMaxDelay( char * buffer, float& delayMin, float& delayMax )
	{
		char* context;
		const char* delims = " ,";
		// ignore the first word (should be "Random" or "R")
		strtok_s(buffer, delims, &context);
		const char* minstr = strtok_s(NULL, delims, &context);
		const char* maxstr = strtok_s(NULL, delims, &context);
		STRCONVERSION_ERROR res = STRCON_SUCCESS;
		res = str2float(delayMin, minstr);
		if (res != STRCON_SUCCESS) return false;
		res = str2float(delayMax, maxstr);
		if (res != STRCON_SUCCESS) return false;
		return true;
	}

	bool IsWhitespace(char c)
	{
		return (c == ' ' || c == '\t' || c == '\n');
	}

	bool Load3DSound(std::ifstream& fin, std::vector<std::unique_ptr<C3DSound>>& sounds, X3DAUDIO_LISTENER* listener)
	{
		char buffer[256];
		
		// ignore empty lines, or lines starting with a '#' (comments)
		do {
			fin.getline(buffer, 256);
		} while (!fin.eof() && (buffer[0] == '\0' || buffer[0] == '#' || IsWhitespace(buffer[0])));

		// Load the filename information, should be the full path from the executable
		const std::string soundFilepath(buffer);
		// check if the filename is a .wav file
		if (soundFilepath.substr(soundFilepath.find_last_of(".") + 1) != "wav") {
			return false;
		}

		// Load the position information, should be on 2nd line in the format "x, y, z" (commas optional)
		float x = 0.0f, y = 0.0f, z = 0.0f;
		fin.getline(buffer, 256);
		if (!LoadPositionFromBuffer(buffer, x, y, z)) {
#ifdef _DEBUG
			std::cout << "Error loading position for " << soundFilepath << std::endl;
#endif//_DEBUG
			return false;
		}

		// Load the volume information
		float volume = 0.0f;
		fin.getline(buffer, 256);
		if (str2float(volume, buffer) != STRCON_SUCCESS) {
#ifdef _DEBUG
			std::cout << "Error loading volume information for " << soundFilepath << std::endl;
#endif//_DEBUG
			return false;
		}

		// Load the pitch information
		float pitch = 0.0f;
		fin.getline(buffer, 256);
		if (str2float(pitch, buffer) != STRCON_SUCCESS) {
#ifdef _DEBUG
			std::cout << "Error loading pitch information for " << soundFilepath << std::endl;
#endif//_DEBUG
			return false;
		}

		// Load the curve distance scalar information
		float curveDistanceScaler = 0.0f;
		fin.getline(buffer, 256);
		if (str2float(curveDistanceScaler, buffer) != STRCON_SUCCESS) {
#ifdef _DEBUG
			std::cout << "Error loading curve distance scalar information for " << soundFilepath << std::endl;
#endif//_DEBUG
			return false;
		}

		// Load whether the sound is looped or played at random intervals
		float delayMin = 0.0f, delayMax = 0.0f;
		fin.getline(buffer, 256);
		if (buffer[0] == 'R') {
			 if (!LoadMinAndMaxDelay(buffer, delayMin, delayMax)) {
#ifdef _DEBUG
				 std::cout << "Error loading delayMin and Max for " << soundFilepath << std::endl;
#endif//_DEBUG
				 return false;
			 }
		} else if (buffer[0] == 'L') {
			;// do nothing
		} else {
#ifdef _DEBUG
			std::cout << "Error loading looped or random line for " << soundFilepath << std::endl;
#endif//_DEBUG
			return false;
		}

		if (fin.bad()) return false;

#ifdef _DEBUG
		std::cout << "Added sound '" << soundFilepath << "' at (" << 
			x << ", " << y << ", " << z << ")." << std::endl;
		std::cout << "\tvolume = " << volume << " pitch = " << pitch << " CurveDistanceScaler = " << 
			curveDistanceScaler << " Delay = " << delayMin << "/" << delayMax << "." << std::endl;
#endif//_DEBUG
		sounds.emplace_back(new C3DSound(soundFilepath, x, y, z, listener));
		sounds.back()->DeactivateUpdate();
		sounds.back()->GetXASound()->SetVolume(volume);
		sounds.back()->GetXASound()->SetPitch(pitch);
		sounds.back()->GetEmitter()->CurveDistanceScaler = curveDistanceScaler;
		sounds.back()->SetInterval(delayMin, delayMax);

		return true;
	}

} // end anonymous namespace.

//--- constructor method.
FishingGame::FishingGame()
	: m_riverSound(nullptr)
	, m_cleanedUp(false)
{ 

} // end FishingGame constructor.

//--- release all the resources allocated by the Initialize function.
//--- No need to delete referenced objects since smart pointers are used.
FishingGame::~FishingGame()
{
	if (!m_cleanedUp) Cleanup();
	m_3DSounds.clear();
} // end FishingGame destructor function.

//=== Implementation of the IState interface.

//--- create and initialize the sounds to be played.
//--- Since this game has no graphics the HWND argument is not used.
bool FishingGame::Initialize (HWND aWindow)
{
	// guard against uninitialized or invalid XACore.
	if (XACore::GetStatus() != XACore::OK) return false;

	m_player.reset(new CPlayer);

	// Create the static sound object; check if ok.
	m_riverSound.reset(new C3DSound ("Sounds/river.wav", 
		c_riverSoundPosition.x, c_riverSoundPosition.y, c_riverSoundPosition.z, 
		m_player->GetListener()));
	if (!m_riverSound->IsValid()) {
		MessageBox (NULL, "Error loading river sound", TEXT ("Initialize() - FAILED"), MB_OK | MB_ICONERROR );
		return false;
	}
	m_riverSound->DeactivateUpdate();
	m_riverSound->GetXASound()->SetLooped(true);
	m_riverSound->GetXASound()->SetVolume (-9.0f);		// native volume is a bit loud.

	m_boulder.reset(new CBoulder(c_boulderPos, c_boulderRadius));

	Load3DSounds(c_3dSoundsInfoFilename);

	std::shared_ptr<XAMusicManager> musicService;

	musicService.reset(new XAMusicManager());
	musicService->AddTrack(IMusicManager::MUSIC_TRACK_DEFAULT, "Sounds/music.wav", 18650, 0);
	musicService->AddTrack(IMusicManager::MUSIC_TRACK_FISH_HOOKED, "Sounds/music_fish_on.wav", 310700, 1823868 - 310700);
	CServiceLocator::provide(musicService);

	return true;		// everything was initialized correctly.
} // end Initialize function.
	
//--- Called at the start of processing; displays instructions and plays the sounds.
bool FishingGame::Setup ()
{
	Instructions();
	// Play the rain sounds and activate the updating.
	//m_riverSound->ActivateUpdate();
	CServiceLocator::getMusicManager()->PlayTrack(IMusicManager::MUSIC_TRACK_DEFAULT);
	for (auto iter =m_3DSounds.begin(); iter!=m_3DSounds.end(); ++ iter)
	{
		(*iter)->ActivateUpdate();
	}
	return true;		// All has been setup without error.
} // end Setup function.

//--- process a single application frame.
//--- Check for key pressess.
//--- this will call the relevant Update() functions on the 3D sounds.
bool FishingGame::ProcessFrame (const float deltaTime)
{
	HandleInputs();

	m_player->Update(deltaTime);
	UpdateRiverSoundPosition();
	m_riverSound->Update(deltaTime);

	// iterate through the list of 3D sounds calling their Update method.
	for (auto iter=m_3DSounds.begin(); iter!=m_3DSounds.end(); ++iter)
	{
		m_boulder->calculateEmitterCone((*iter)->GetEmitter());
		(*iter)->Update(deltaTime);
	}

	CServiceLocator::getMusicManager()->Update(deltaTime);

	return true;
} // end ProcessFrame function.

//--- Stop the sounds playing.
//--- set the clean up flag to true.
//--- this will be called by the destructor if not previously called explicitly.
void FishingGame::Cleanup ()
{
	m_riverSound->DeactivateUpdate();
	for (auto iter=m_3DSounds.begin(); iter!=m_3DSounds.end(); ++iter)
	{
		(*iter)->DeactivateUpdate();
	}
	CServiceLocator::clearMusicManager();
	m_cleanedUp = true;
} // end Cleanup function.


//=== Implementation of application specific functions.

void FishingGame::Load3DSounds( const std::string& filename )
{
	// clear any currently existing 3d sounds
	if (!m_3DSounds.empty()) {
#ifdef _DEBUG
		std::cout << "Reloading sounds..." << std::endl;
#endif//_DEBUG
		m_3DSounds.clear();
	}

	std::ifstream fin (filename);
	if (fin.is_open()) {
		while (!fin.eof()) {
			if (!Load3DSound(fin, m_3DSounds, m_player->GetListener())) {
				break;
			}
		}

		AddUnique3DSounds();

#ifdef _DEBUG
		std::cout << "Added all sounds from '" << filename << 
			"'. There are " << m_3DSounds.size() << " sound(s) in the scene" << std::endl; 
#endif//_DEBUG
	} else {
#ifdef _DEBUG
		std::cout << "Could not open " << filename << " to load 3D sounds." << std::endl;
#endif//_DEBUG
	}
}

void FishingGame::UpdateRiverSoundPosition()
{
	const X3DAUDIO_VECTOR& playerPosition = m_player->GetPosition();
	m_riverSound->SetPosition(playerPosition.x + c_riverSoundPosition.x, 
		playerPosition.y + c_riverSoundPosition.y, playerPosition.z + c_riverSoundPosition.z);
}

void FishingGame::HandleInputs()
{
	unsigned short flags = 0x0001;
	//--- display the instructions
	if (GetAsyncKeyState ('I') & flags) {
		Instructions();
	}
	//--- cast the fishing line if it has not already been cast and if the player is not moving
	if (GetAsyncKeyState('F') & flags) {
		m_player->Cast();
	}
	//--- reel in the line if it has been cast
	if (GetAsyncKeyState('R') & flags) {
		m_player->ReelIn();
	}
	//--- yank left if the fish is swimming right
	if (GetAsyncKeyState('A') & flags) {
		m_player->YankLeft();
	}
	//--- yank left if the fish is swimming right
	if (GetAsyncKeyState('D') & flags) {
		m_player->YankRight();
	}
	//--- mute or unmute the music
	if (GetAsyncKeyState('M') & flags) {
		CServiceLocator::getMusicManager()->ToggleMute();
	}
	//--- move the player up the river
	if (GetAsyncKeyState(VK_UP) & flags) {
		m_player->MoveUpRiver();
	}
	//--- move the player down the river
	if (GetAsyncKeyState(VK_DOWN) & flags) {
		m_player->MoveDownRiver();
	}
	//--- reload the 3d sounds from the 3dSounds.txt file and play them
	if (GetAsyncKeyState('K') & flags) {
		Load3DSounds(c_3dSoundsInfoFilename);
		for (auto it = m_3DSounds.begin(); it != m_3DSounds.end(); ++it) {
			(*it)->ActivateUpdate();
		}
	}
}

void FishingGame::AddUnique3DSounds()
{
	// bird sound flying around the player's head
	m_3DSounds.emplace_back( new CBird(0.0f, 5.0f, 0.0f, m_player->GetListener(), 10.0f, 2.0f));
	m_3DSounds.back()->DeactivateUpdate();
}

//=== end of code.