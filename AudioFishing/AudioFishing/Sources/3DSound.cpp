/*
	File:	C3DSound.cpp
	Version:	1.1
	Date:	18th February 2014.
	Author:	Allan C. Milne.

	Exposes: implementation of the C3DSound class.
	Requires:	XACore, XASound, Updateable.
	
	Description:
	This is part of a solution to the Soundscape3 practical;
	
	this is the implementation of the C3DSound class
	* see C3DSound.hpp for details.
*/

// system includes.
#include <Windows.h>
#include <X3DAudio.h>
#include <memory>
#include <string>
using namespace std;

// Framework includes.
#include "Updateable.hpp"
#include "XACore.hpp"
#include "XASound.hpp"
using AllanMilne::Updateable;
using AllanMilne::Audio::XACore;
using AllanMilne::Audio::XASound;

#include "3DSound.hpp"

namespace {
	float randFloat(const float lo, const float hi) {
		return lo + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(hi-lo))); 	
	}
}

//=== Class specific behaviour.

//--- constructor: creates XASound object for supplied .wav file and emitter for supplied position.
C3DSound::C3DSound (const string& aWavFile, const float x, const float y, const float z, X3DAUDIO_LISTENER* aListener)
	: m_randomMin(0.0f)
	, m_randomMax(0.0f)
	, m_timeSinceLastPlayed(0.0f)
	, m_randomIntervalsEnabled(false)
{
	// Create the sound element.
	m_sound = make_shared<XASound>(aWavFile, true);			// true= filters enabled.
	if (!m_sound->IsValid()) return;		// Can do no more.
	m_sound->SetLooped(true);
	// Set up the emitter struct.
	SecureZeroMemory (&m_emitter, sizeof(X3DAUDIO_EMITTER));
	XAUDIO2_VOICE_DETAILS details;
	m_sound->GetSourceVoice()->GetVoiceDetails(&details);
	m_emitter.ChannelCount = details.InputChannels;
	m_emitter.CurveDistanceScaler = 15.0f;
	m_emitter.Position.x = x;
	m_emitter.Position.y = y;
	m_emitter.Position.z = z;
	// Point back to the listener being used.
	m_listener = aListener;
} // end C3DSound constructor.

//--- Destructor: stop sound playing; no deletion required since smart pointers used.
C3DSound::~C3DSound()
{
	m_sound->Stop();
} // end C3DSound destructor.

//=== Implementation of the Updateable interface.

//--- when activated the sound should begin playing.
void C3DSound::OnUpdateActivation ()
{
	if (!m_randomIntervalsEnabled) {
		m_sound->Play();
	}
} // end OnUpdateActivation function.

//--- When deactivated the sound should stop playing.
void C3DSound::OnUpdateDeactivation ()
{
	m_sound->Stop();
} // end OnUpdateDeactivation function.

//--- Called every frame to recalculate and set DSP settings for the sound position; deltaTime not used.
void C3DSound::OnUpdate (const float deltaTime)
{
	XACore::GetInstance()->Apply3D ( m_sound->GetSourceVoice(), &m_emitter, m_listener, X3DAUDIO_CALCULATE_MATRIX | X3DAUDIO_CALCULATE_LPF_DIRECT);
	if (m_randomIntervalsEnabled) {
		m_timeSinceLastPlayed += deltaTime;
		if (m_timeSinceLastPlayed >= m_nextPlayed) {
			m_timeSinceLastPlayed = 0.0f;
			m_sound->Play();
			m_nextPlayed = randFloat(m_randomMin, m_randomMax);
		}
	}
} // end OnUpdate function.

//--- getter and setter for the sound position.
void C3DSound::SetPosition (const float x, const float y, const float z)
{
	m_emitter.Position.x = x;
	m_emitter.Position.y = y;
	m_emitter.Position.z = z;
} // end SetPosition function.

void C3DSound::SetInterval( float aMin, float aMax )
{
	if (aMin == 0.0f && aMax == 0.0f) {
		m_sound->GetBuffer().LoopCount = XAUDIO2_LOOP_INFINITE;
		m_randomIntervalsEnabled = false;
	} else {
		m_sound->GetBuffer().LoopCount = 1;
		// do not allow the values to be under 0.0f
		m_randomIntervalsEnabled = true;
		// protect against cases where the min value is greater than the max value
		if (aMin > aMax) {
			m_randomMin = max(0.0f, aMax);
			m_randomMax = max(0.0f, aMin);
		} else {
			m_randomMin = max(0.0f, aMin);
			m_randomMax = max(0.0f, aMax);
		}
		m_timeSinceLastPlayed = 0.0f;
		m_nextPlayed = randFloat(m_randomMin, m_randomMax);
	}
}

//=== end of code.