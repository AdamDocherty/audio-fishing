#include "Player.h"

// system includes
#include <iostream>

// framework includes
#include "XASound.hpp"
#include "StuVector3.hpp"

#include "Bobber.h"

using AllanMilne::Audio::XASound;

namespace {
	const X3DAUDIO_VECTOR c_locationPosition[CPlayer::LOCATION_count] = {
		{ -15.0f, 1.0f, 0.0f }, // near waterfall
		{ 0.0f, 1.0f, 0.0f }, // middle of river/behind rock
		{ 10.0f, 1.0f, 0.0f } // bottom of the river
	};

	//--- the movement speed of the player in meters per second
	const float c_defaultMovementSpeed = 4.0f;

	const std::string c_footstepsSoundFilename = "Sounds/footsteps.wav";

	//--- the max reel in speed in meters per second
	const float c_maxReelInSpeed = 5.0f;
	//--- reel in speed deceleration rate in meters per second per second
	const float c_reelInDeceleration = 1.7f;
	//--- the amount the reel in speed will increase for each hit of the button
	const float c_reelInSpeedBoost = 0.3f;
	//--- the linear interpolation constant to be used for rotating the player
	//--- must be in the range (0.0f, 1.0f]
	const float c_lerpConstant = 0.1f;

	//--- The default position of where the cast will land (relative to the player)
	const X3DAUDIO_VECTOR c_defaultCastPosition = { 0.0f, 0.0f, 10.0f };
	//--- The accuracy of the cast, the higher the number the further away from the
	// default cast position the bobber can reach.
	const float c_castError = 2.0f;

	float randFloat(const float lo, const float hi) {
	    return lo + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(hi-lo))); 	
	}

	float lerp(const float v0, const float v1, const float t) {
		return (1.0f - t)*v0 + t*v1;
	}
}

CPlayer::CPlayer()
	: m_reelInSpeed(0.0f)
	, m_movementSpeed(c_defaultMovementSpeed)
	, m_location(LOCATION_MIDDLE_OF_RIVER)
{
	SecureZeroMemory(&m_listener, sizeof X3DAUDIO_LISTENER);
	// Start at the origin.
	SetPosition(c_locationPosition[m_location].x, c_locationPosition[m_location].y, c_locationPosition[m_location].z);
	m_listener.OrientFront.x = 0.0f;
	m_listener.OrientFront.y = 0.0f;
	m_listener.OrientFront.z = 1.0f;
	// Will always be upright; no pitch or roll.
	m_listener.OrientTop.x = 0.0f;
	m_listener.OrientTop.y = 1.0f;
	m_listener.OrientTop.z = 0.0f;

	m_bobber.reset(new CBobber(&m_listener));

	m_footstepsSound.reset(new XASound(c_footstepsSoundFilename));
	m_footstepsSound->SetLooped(true);

	// the player should update at 60 fps
	SetUpdateFrameTime(0.16f);
}

void CPlayer::OnUpdate( const float deltaTime )
{
	// update the reel in speed if the bobber has been cast
	if (m_bobber->GetState() == CBobber::BOBBER_STATE_CAST ||
	    m_bobber->GetState() == CBobber::BOBBER_STATE_FISH_HOOKED) {
		m_bobber->SetReelInSpeed(m_reelInSpeed);

		m_reelInSpeed -= (c_reelInDeceleration * deltaTime);
		m_reelInSpeed = max(0.0f, m_reelInSpeed);
	} else {
		// otherwise stop reeling in
		m_reelInSpeed = 0.0f;
	}

	if (isMoving()) {
		Move(deltaTime);
	} else {
		const X3DAUDIO_VECTOR forwardVector = { 0.0f, 0.0f, 1.0f };
		UpdateOrientation(forwardVector, deltaTime);
	}

	m_bobber->Update(deltaTime);
}

void CPlayer::SetPosition(float x, float y, float z)
{
	m_listener.Position.x = x;
	m_listener.Position.y = y;
	m_listener.Position.z = z;
}

void CPlayer::Cast()
{
	X3DAUDIO_VECTOR bobberPos = { 
		GetPosition().x + c_defaultCastPosition.x + randFloat(-c_castError, c_castError), 
		0.0f, // land at y=0.0f, level of the water
		GetPosition().z + c_defaultCastPosition.z + randFloat(-c_castError, c_castError) 
	};
	m_bobber->Cast(bobberPos);
}

void CPlayer::ReelIn()
{
	m_reelInSpeed += c_reelInSpeedBoost;
	m_reelInSpeed = min(m_reelInSpeed, c_maxReelInSpeed);
}

bool CPlayer::isMoving() const
{
	// if the current location is not equal to the target location, the player is moving
	return !(GetPosition().x == c_locationPosition[m_location].x &&
		GetPosition().y == c_locationPosition[m_location].y &&
		GetPosition().z == c_locationPosition[m_location].z);
}

void CPlayer::Move( const float deltaTime )
{
	const stu::v3f currentPosition = { GetPosition().x, GetPosition().y, GetPosition().z };
	const stu::v3f targetPosition = { c_locationPosition[m_location].x, c_locationPosition[m_location].y, c_locationPosition[m_location].z };

	const stu::v3f diff = (targetPosition - currentPosition);
	stu::v3f velocity = diff;
	velocity.Normalise();
	velocity *= deltaTime * m_movementSpeed;

	if (velocity.isZero() || velocity.MagnitudeSquared() > diff.MagnitudeSquared()) {
		SetPosition(targetPosition.x, targetPosition.y, targetPosition.z);
		OnLocationReached();

	} else {
		const stu::v3f newPosition = currentPosition + velocity;
		velocity.Normalise();
		const X3DAUDIO_VECTOR forwardVector = { velocity.x, velocity.y, velocity.z };
		UpdateOrientation(forwardVector, deltaTime);
		SetPosition(newPosition.x, newPosition.y, newPosition.z);
	}
}

void CPlayer::MoveUpRiver()
{
	// don't move if the player is fishing or already moving
	if (isMoving() || isFishing()) return;
	if (m_location == 0) {
		// if the player is at the top of the river
		return;
	} else {
		// move up one spot (lower numbers are closer to the top of the river)
		m_location--;
		OnStartMoving();
	}
}

void CPlayer::MoveDownRiver()
{
	// don't move if the player is fishing or already moving
	if (isMoving() || isFishing()) return;
	if (m_location == LOCATION_count - 1) {
		// if the player is at the bottom of the river
		return;
	} else {
		// move down one spot (higher numbers are closer to the bottom of the river)
		m_location++;
		OnStartMoving();
	}
}

bool CPlayer::isFishing() const
{
	const CBobber::BobberState s = m_bobber->GetState();
	return !(s == CBobber::BOBBER_STATE_NOT_CAST);
}

void CPlayer::OnLocationReached()
{
#ifdef _DEBUG
	std::cout << "New location reached... current position is (" << GetPosition().x << ", " << GetPosition().y << ", " << 
		GetPosition().z << ") at location " << m_location << "." << std::endl;
#endif//_DEBUG
	m_footstepsSound->Stop();
}

void CPlayer::OnStartMoving()
{
	m_footstepsSound->Play();
}

void CPlayer::UpdateOrientation( const X3DAUDIO_VECTOR& direction, const float deltaTime )
{
	float currentAngle = atan2f( m_listener.OrientFront.z, m_listener.OrientFront.x );
	float newAngle = atan2f ( direction.z, direction.x );
	newAngle = lerp(currentAngle, newAngle, c_lerpConstant);

	//--- uncomment this section for debug information when rotating
//#ifdef _DEBUG
	//if (currentAngle != newAngle) {
	//	std::cout << "Current angle = " << currentAngle << " (" << m_listener.OrientFront.x << ", " << m_listener.OrientFront.z << ")" <<
	//		" New angle = " << newAngle << " (" << cos(newAngle) << ", " << sin(newAngle) << ")" << std::endl;
	//}
//#endif//_DEBUG

	m_listener.OrientFront.x = cos(newAngle);
	m_listener.OrientFront.z = sin(newAngle);
}

void CPlayer::YankLeft()
{
	m_bobber->YankLeft();
}

void CPlayer::YankRight()
{
	m_bobber->YankRight();
}