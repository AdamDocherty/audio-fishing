# Audio Fishing

## Overview
#### Scene description
The scene is a depiction of a riverside with a waterfall at one end, a large boulder in the middle of the river, and various wildlife populating the area.

#### User Interactions
The user can move up or down the river to pre-defined points using the up and down arrow keys.
The user can toggle the music on or off at any time by hitting the 'M' key.

The user can start the fishing mini-game by hitting the 'F' key when they are standing still:

*  A short while after the line has been cast, the user will hear the sound of the bobber landing in the water.
*  The user can reel in the line at any time with the 'R' key.
*  After a short while, a fish will bit the line and start splashing around (this will also change the music).
*  While reeling in the fish, it will try and escape by moving either left or right.
*  The user must yank the rod to pull the fish back in to the middle.
*  If the fish is swimming left, the user must hit 'D' to yank the rod to the right; if the fish is swimming right, the user must hit 'A' to yank the rod to the left.
*  If the fish gets too far to either side, it will escape and the line will snap.
*  Once the fish has been fully reeled in it will be pulled out of the water.

#### Dynamic Audio Elements
* Fishing bobber (float)
	* Makes a sound when the bobber lands in the water:
	* Makes a constant 3D sound at the location of the bobber when a fish is on the line.
	* A sound is played when the rod is yanked to either side to pull the fish back in to the middle.
	* A sound is played if the line breaks.
	* A sound is played when the fish is reeled in.
* Footsteps when the player is moving.
* A bird flying overhead in a circle.
* Music which changes depending on the state of the game:
	* One track for the normal state.
	* One track for when the fish is on the line.
	* The tracks are changed using a cross-fade. 
* Various other environmental sounds such as crickets and frogs, which are played randomly at distinct locations. 
* A boulder in the middle of the river will block the sounds of anything behind it.

## C++ source code structure
The program was built using Allan C. Milne's Audio Framework and is an extension of the Soundscape3 tutorial. The StuMath maths library by Stuart Milne was also used for Vector math.

Excluding files in the Framework, all concrete classes are prefixed with 'C' and all interfaces are prefixed with 'I'; member variables are prefixed with 'm_', static variables with 's_', and constants with 'c_' (most constants are stored in an anonymous namespace in the implementation file). 

#### Overall Architecture & Class Relationships
The class which implements the **IState** interface is the **CFishingGame** class and it is the main game loop of the program. It contains the river sound, an array of **C3DSound** pointers representing various objects in the scene, the boulder, and the player.

The **CPlayer** class implements the **Updateable** class and contains the XAudio2 listener struct which can be accessed through a public getter.
It also containts the sound effects for the footsteps, and an instance of the CBobber class.
The application checks for inputs in the *HandleInputs* function in **CFishingGame**, most of these inputs will make a call to one of the players event methods.

The **CBobber** class implements the **Updateable** class and represents the end of the fishing line. The state of the bobber is represented by a heirachical state machine: 
There is the bobber state which can be in one of four states (Not cast, in flight, in water, and fish hooked).
The fish hooked state is then further divided up in to 5 states representing the current behaviour of the fish (still, swim left, swim right, yanked left, and yanked right).
Similarly to the player there are event function that, when called from outside the class, trigger behaviours. 
The bobber contains several sounds: the splash sound for when the bobber enters the water, the reel in sound, the caught sound for when the fish is pulled out of the water, the yank sound for when the rod is yanked to the side, the line snapping sound, and the sound of the fish splashing.

The **CBoulder** class is used for representing a spherical object which blocks sound.
It is initialised with a radius and position.
Every frame, any sounds that can be blocked by the boulder must call the boulder's *CalculateEmitterCone* function, which will correctly set the *pCone* member of the emitter.
Each object can only be blocked by a single **CBoulder** so only one should be present in the scene.

The music manager is responsible for playing music and swapping between tracks. 
It is implemented first as an interface (**IMusicManager**) and also an implementation (**XAMusicManager**). 
The music manager can be accessed through it's interface from the **CServiceLocator** class which uses the Service Locator pattern to decouple it from the classes which change the music (such as the **CBobber** class). 
The music tracks must be added to the **XAMusicManager** object when it is created, this is done in the Initialize function in **CFishingGame**. 

### Implementation

#### Framework Modifications
A *RedirectIOToConsole* function was added to the Main_AudioFishing.cpp file to allow debugging information to be output to the console.

The default value of the **Updateable** class' *mFrameTime* was changed from 0.0f to 0.016f to make all classes update at 60FPS a default. This was only an important change for the easing function implementations as most other gradual changes in update functions make use of the *deltaTime* argument.

#### Static 3D sounds
The **C3DSound** class is a slight modification of the **My3DSound** class in the Soundscape3 example provided by Allan Milne. It is a very basic class that 

Since there are a lot of non-moving 3D sounds in the scene -- such as the waterfall, frogs and crickets -- a load from file function was implemented.
The sound definitions are stored in the file '*3dSounds.txt*', in the format described at the top of FishingGame.h. 
This allows the user to define the position, volume, pitch, curve distance scaler and looping behaviour of each sound without having to recompile. 
Additionally, when running the program in Debug mode, pressing 'K' will reload all the sounds allowing fine tuning of the sounds while the game is running.
All static 3D sounds are stored in the *m_3DSounds* array in **CFishingGame**, so they will all have the *pCone* set by the boulder every frame.

#### Bird sound
The **CBird** class extends the **C3DSound** class so therefore it has all the same properties as a regular 3D sound.
This time the *OnUpdate* method is used to move the bird around a centre point at a given radius.
It is also stored in the *m_3DSounds* array and therefore will be affected by the boulder.
Since there is no syntax for loading in the bird using the text file, the bird is added in code in the function *AddUnique3DSounds*.

#### Boulder
The **CBoulder** class stores a position and a radius.
When an emitter is passed in to the *CalculateEmitterCone* function, the vector between the emitter and the boulder are calculated using the boulder's position and the *Position* property of the emitter.
From this the angle of the cone is calculated and the *OrientFront* property of the emitter is set.
The cones are set so the inner volume is less than one, and the outer volume is equal to one, meaning when the listener is outside the cone there will be no effect on the audio.

One problem with the boulder implementation is that the generated cone has no distance component, meaning if the boulder is behind the player it may block sounds coming from in front of the player.
This problem is solved by placing the boulder in a position where it will never be behind the player, but it could be fixed in the long term by performing a ray cast first before setting the sound cone. 

#### Music
The **XAMusicManager** class is an implementation of the **IMusicManager** interface.
In the **IMusicManager** there is an enumeration for each track that can be played.
When the **XAMusicManager** instance is created, it must be provided with the filepaths of the music tracks as well as the *LoopBegin* and *LoopLength* properties to be used by the XAudio2 Buffer (the correct *LoopBegin* and *LoopLength* were found by trail and error using Audacity).
The tracks are then stored in a collection of **CMusicTrack** pointers.
The **XAMusicManager** also stores pointers to the currently playing track and the previous track, a pointer to the previous track is required so it's volume can continuously be reduced as it is fading out. 

#### Sound Randomiser
The **CSoundRandomiser** class is derived mostly from the **XASound** class, with a minor change to allow multiple sound files to be played from the same source voice.
Once this functionality was added, the Play function was changed to submit a random Source Buffer.
When the sound is being looped, a random sound is added to the end of the queue whenever the number of buffers queued is two or less.

#### Service Locator
The **CServiceLocator** provides access to any services that need to be available to the entire game.
In this case, the only service provided is the Music Manager.
An implementation of the **IMusicManager** interface is provided to the Service Locator with the *Provide* method, afterwards any class can get access to the interface with the *GetMusicManager* function.
Using a service provider gives the advantage of being able to have blank implementations (an implementation of the **IMusicManager** interface with all methods doing nothing) to completely remove audio without having to change any code anywhere else in the project.

#### Player
The **CPlayer** class extends the **Updateable** class, so is updated every frame. 
The main responsibility of the player class is moving the player around the scene, and passing events down to the **CBobber** object. They player is also responsible for calling **CBobber**'s Update function.

Each possible location for the player is represented by an enumeration (starting at 0 for the top of the river), an array of 3D Vectors is then used to give each of these locations a 3D position. 
When the *MoveUpRiver* event is triggered (up arrow) the player checks if it's already at the top of the river, if not then it will start walking to the next location up the river (and vice-versa for the *MoveDownRiver* event).
The index of the players current location is stored as an integer rather than an enumerated type so that integer arithmetic can be performed on it.

The speed that the line is reeled in is also controlled by the player.
Repeatedly tapping the 'R' key will reel in the line faster, over time however the reel in speed will decrease.
As the reel in speed increases, the pitch of the reel in sound also increases.

#### Bobber
The **CBobber** class also extends the **Updateable** class.
The *OnUpdate* method is laid out as a simple finite state machine. All the possible states are defined as enumerations and a switch block is used to call the relevant update method.
The *UpdateFishHooked* method follows a similar pattern with all the possible states for the fish, but rather than separate functions for each state, the code is in the case blocks as it is never more than four lines.

The behaviour of the fish on the line is also controlled by the **CBobber** class.
The fish starts in the 'still' state, after one second it will start moving either left or right, the player must then yank the rod in the opposite direction to stop the fish from getting too far away and breaking the line.
When the fish is swimming away, it moves along the x-axis; when the fish is yanked back in it moves back towards the middle fairly quickly until the difference between the bobber's x position and the players x position is about zero.

### Alternative approaches

* A vector library that used the **X3DAUDIO_VECTOR** struct rather than a seperate struct would have been useful for performing vector math without having to translate between the X3DAudio and the **stu::v3f** structs.
* Rather than having the Fish State information be part of the **CBobber** class, having the Fish information be it's own class would have made more sense and would have allowed for some more unique behaviour.